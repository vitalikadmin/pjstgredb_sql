<?php

$dbconn = pg_connect ("host=localhost port=5432 dbname=test_db1 user=postgres password=AS131183as")
or die('Не удалось соединиться: ' . pg_last_error());

//Основные функции PHP для работы с PostgreSQL
//pg_connect – открывает соединение с базой данных, возвращает указатель соединения.
//pg_query – выполняет запрос к базе данных, возвращает результат запроса.
//pg_fetch_assoc – преобразовывает результат запроса в ассоциативный массив.
//pg_close – закрывает соединение с базой данных.

$query = 'SELECT * FROM  workers';
$result = pg_query($query) or die('Ошибка запроса: ' . pg_last_error());

// Вывод результатов в HTML
echo "<table>\n";
while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
    echo "\t<tr>\n";
    foreach ($line as $col_value) {
        echo "\t\t<td>$col_value</td>\n";
    }
    echo "\t</tr>\n";
}
echo "</table>\n";

// Очистка результата
pg_free_result($result);

// Закрытие соединения
pg_close($dbconn);
?>