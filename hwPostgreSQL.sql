-- Есть таблица - http://joxi.ru/n2YeR8QTb7qPz2
-- Все манипуляции будут в ней, если не указано иного.
-- Все выборки и sql-команды нужно писать в файлах с расширением “sql”, к примеру “example.sql” (тогда у вас будет подсветка синтаксиса).
--
-- Задача 1:
-- Напишите sql-команду для создания таблицы workers, как на скриншоте выше;
-- После создания таблицы, добавьте к ней колонку last_login, типа int, которая не может быть пустой;
-- Добавьте еще одну колонку test произвольного типа и удалите ее;
-- Переименуйте колонку last_login в last_login_time;
-- Изменить дефолтное значение last_login_time на 0;
-- Измените колонку last_login_time, чтобы она теперь могла быть пустой;
-- Заполните таблицу workers данными (в один запрос), как на скриншоте, можете добавить дополнительных данных.
-- Измените поля с id 2 и 4, вставьте в них текущее Unix-время;
-- Создайте 2 комментария к любым (выбранным вами) полям.
--
-- Задача 2: Выбрать работника с id = 3
-- Задача 3: Выбрать работников с зарплатой 1000$.
-- Задача 4: Выбрать работников в возрасте 23 года.
-- Задача 5: Выбрать работников с зарплатой более 400$.
-- Задача 6: Выбрать работников с зарплатой равной или большей 500$.
-- Задача 7: Выбрать работников с зарплатой НЕ равной 500$.
-- Задача 8: Выбрать работников с зарплатой равной или меньшей 900$.
-- Задача 9: Узнайте зарплату и возраст Васи.
-- Задача 10: Выбрать работников в возрасте от 25 (не включительно) до 28 лет (включительно).
-- Задача 11: Выбрать работника Петю.
-- Задача 12: Выбрать работников Петю и Васю.
-- Задача 13: Выбрать всех, кроме работника Петя.
-- Задача 14: Выбрать всех работников в возрасте 27 лет или с зарплатой 1000$.
-- Задача 15: Выбрать всех работников в возрасте от 23 лет (включительно) до 27 лет (не включительно) или с зарплатой 1000$.
-- Задача 16: Выбрать всех работников в возрасте от 23 лет до 27 лет или с зарплатой от 400$ до 1000$.
-- Задача 17: Выбрать всех работников в возрасте 27 лет или с зарплатой не равной 400$.
-- Задача 18 (на UPDATE, 27 строка в файле с командами вам подскажет):
-- Поставьте Васе зарплату в 200$.
-- Работнику с id=4 поставьте возраст 35 лет.
-- Измените поля с id 2 и 4, вставьте в них текущее Unix-время;
-- Задача 19: (на DELETE, 28 строка в файле с командами вам подскажет):
-- Удалите Колю.
-- Удалите всех работников, у которых возраст 23 года.

--TASK 1 ;


CREATE TABLE workers(  --создаем таблицу
                        id SERIAL PRIMARY KEY,
                        username VARCHAR (50) UNIQUE NOT NULL,
                        created_time integer NOT NULL,
                        updated_time TIMESTAMP,
                        age integer,
                        salary integer NOT NULL
);

INSERT INTO workers (username, created_time, age, salary) --заполняем таблицу данными в один запрос
VALUES ('Дима', 1, 23, 400),
       ('Петя', 2, 25, 500),
       ('Вася', 3, 23, 500),
       ('Коля', 4, 30, 1000),
       ('Иван', 5, 27, 500),
       ('Лирил',6, 28, 1000);

ALTER TABLE workers ADD COLUMN last_login integer; -- Добавляем колонку 'last_login'

ALTER TABLE workers ALTER COLUMN last_login DROP NOT NULL; --устанавливаем значения не нул

ALTER TABLE workers ADD COLUMN test varchar (20); --добавляем колонку 'test'

ALTER TABLE workers DROP COLUMN test; --удаляем колонку 'test'

ALTER TABLE workers RENAME COLUMN last_login TO last_login_time; -- переименовываем колонку "last_login TO last_login_time;"

ALTER TABLE workers ALTER COLUMN last_login_time SET DEFAULT 0; --устанавливаем дефолтное значение на 0

ALTER TABLE workers ALTER COLUMN last_login_time DROP DEFAULT; --удаляем дефолтное значение

ALTER TABLE workers ALTER COLUMN last_login_time drop NOT NULL; --устанавливаем значения не нул

ALTER TABLE workers drop COLUMN updated_time; --удалили колонку с типом инт

 ALTER TABLE workers ADD COLUMN updated_time timestamp; --создали новую колонку с типом таймстемп

update workers set updated_time = CURRENT_TIMESTAMP where (id = 2 or id = 4); --обновили или изменили поля

--Задача 2: Выбрать работника с id = 3

SELECT * from workers where  id = 3; --Выбираем работника с id = 3

-- Задача 3: Выбрать работников с зарплатой 1000$.

SELECT * FROM workers WHERE salary = 1000; --Выбираем работников с зарплатой 1000$.

-- Задача 4: Выбрать работников в возрасте 23 года.

SELECT * FROM workers WHERE age = 23; --Выбираем работников в возрасте 23 года.

-- Задача 5: Выбрать работников с зарплатой более 400$

SELECT * FROM workers WHERE salary > 400; --Выбираем работников с зп больше 400;

SELECT * FROM workers WHERE salary < 500; --Выбираем работников с зарплатой НЕ равной 500$.

-- Задача 7: Выбрать работников с зарплатой НЕ равной 500$

Select * from workers where salary != 500; --Выбираем работников с зарплатой НЕ равной 500$

-- Задача 8: Выбрать работников с зарплатой равной или меньшей 900$.

Select * from workers where salary = 900 and salary < 900; --Выбираем работников с зарплатой равной или меньшей 900$.

-- Задача 9: Узнайте зарплату и возраст Васи.
\\\\\\

-- Задача 10: Выбрать работников в возрасте от 25 (не включительно) до 28 лет (включительно).

select * from workers WHERE age <= 25 AND age <= 28; --Выбрать работников в возрасте от 25 (не включительно) до 28 лет (включительно).

-- Задача 11: Выбрать работника Петю.

select * from workers WHERE username = 'Петя'; --Выбрали работника Петю.

-- Задача 12: Выбрать работников Петю и Васю.

select * from workers WHERE username = 'Петя' or username = 'Вася'; --Вибираем работников Петю и Васю.

-- Задача 13: Выбрать всех, кроме работника Петя.

select * from workers WHERE username != 'Петя'; --Выбрали всех, кроме работника Петя.

-- Задача 14: Выбрать всех работников в возрасте 27 лет или с зарплатой 1000$.

SELECT username, age from workers WHERE salary <= 1000; --Выбираем всех работников в возрасте 27 лет или с зарплатой 1000$.

-- Задача 15: Выбрать всех работников в возрасте от 23 лет (включительно) до 27 лет (не включительно) или с зарплатой 1000$.

SELECT username, age >= 23 and age <= 27 from workers WHERE salary <= 1000; --Выбираем всех работников в возрасте от
    -- 23 лет (включительно) до 27 лет (не включительно) или с зарплатой 1000$.

-- Задача 16: Выбрать всех работников в возрасте от 23 лет до 27 лет или с зарплатой от 400$ до 1000$.

SELECT username, age >= 23 and age <= 27 from workers WHERE salary = 400 and salary = 1000; --ВЫбрали всех работников
    -- в возрасте от 23 лет до 27 лет или с зарплатой от 400$ до 1000$.

-- Задача 17: Выбрать всех работников в возрасте 27 лет или с зарплатой не равной 400$.

SELECT username, age = 27 from workers WHERE salary != 400; --ВЫьрали всех работников в возрасте 27 лет
    -- или с зарплатой не равной 400$

-- Задача 18 (на UPDATE, 27 строка в файле с командами вам подскажет):
-- Поставьте Васе зарплату в 200$.

UPDATE workers SET salary = 200 WHERE username = 'Вася'; --Поставил З.П Василию  = 200

-- Работнику с id=4 поставьте возраст 35 лет.

UPDATE workers SET id = 4 WHERE age = 35; --Работнику с айди 4 поставил возвраст 35

-- Измените поля с id 2 и 4, вставьте в них текущее Unix-время;????? Спросить показат решение ((( Не смог толком понять ....

-- Задача 19: (на DELETE, 28 строка в файле с командами вам подскажет):
-- Удалите Колю.

DELETE FROM workers WHERE username='Коля'; --Удалил Коляна

-- Удалите всех работников, у которых возраст 23 года.

DELETE FROM workers WHERE age = 23; --Удалил всех работничкев с возрастом 23 года)))

-- CREATE TABLE workers(
--                         id SERIAL PRIMARY KEY,
--                         username VARCHAR (50) UNIQUE NOT NULL,
--                         email VARCHAR (355) UNIQUE NOT NULL,
--                         created_time integer NOT NULL,
--                         updated_time integer,
--                         age integer,
--                         salary integer NOT NULL
-- );
--
-- COMMENT ON COLUMN workers.age is 'Stores the age of worker.';
--
-- CREATE INDEX workers_age_idx ON workers (age);
--
-- --DROP TABLE IF EXISTS workers;
-- INSERT INTO workers (username, email, created_time, age, salary) VALUES ('Гена', 'test0@gmail.com', 1555494330, 30, 1000);
-- INSERT INTO workers (username, email, created_time, age, salary)
-- VALUES ('Петя', 'test1@gmail.com', 1555494331, 20, 2000),
--        ('Вася', 'test12@gmail.com', 1555494332, 25, 3000),
--        ('Иван', 'test13@gmail.com', 1555494333, 35, 4000),
--        ('Коля', 'test14@gmail.com', 1555494334, 40, 5000),
--        ('Никита', 'test15@gmail.com', 1555494335, 50, 6000),
--        ('Юля', 'test16@gmail.com', 1555494336, 18, 6000),
--        ('Оля', 'test17@gmail.com', 1555494337, 18, 5000),
--        ('Карина', 'test18@gmail.com', 1555494338, 18, 4000),
--        ('Вероника', 'test19@gmail.com', 1555494339, 18, 3000);
-- --UPDATE workers SET updated_time = 1555494340 WHERE username = 'Вася';
-- --DELETE FROM workers WHERE username='Петя';
-- --ALTER TABLE workers ADD COLUMN card_id integer;
--
-- CREATE TABLE cards(
--                       id SERIAL PRIMARY KEY,
--                       status_id integer NOT NULL,
--                       card_code bigint UNIQUE NOT NULL,
--                       created_time integer NOT NULL,
--                       updated_time integer
-- );
-- --DROP TABLE IF EXISTS cards;
-- alter table workers
--     add constraint fk_cards_workers
--         foreign key (card_id)
--             REFERENCES cards (id)
--                 MATCH FULL
--             ON DELETE Cascade
--             ON UPDATE Cascade;
--
-- INSERT INTO cards (status_id, card_code, created_time)
-- VALUES (1, 4444111155552220, 1555494331),
--        (1, 4444111155552221, 1555494331),
--        (2, 4444111155552222, 1555494331),
--        (2, 4444111155552223, 1555494331),
--        (3, 4444111155552224, 1555494331),
--        (4, 4444111155552225, 1555494331),
--        (1, 4444111155552226, 1555494331),
--        (1, 4444111155552227, 1555494331),
--        (1, 4444111155552228, 1555494331),
--        (1, 4444111155552229, 1555494331),
--        (1, 4444111155552230, 1555494331);
--
-- UPDATE workers SET card_id = 1 WHERE id = 1;
-- UPDATE workers SET card_id = 2 WHERE id = 2;
-- UPDATE workers SET card_id = 3 WHERE id = 3;
-- UPDATE workers SET card_id = 4 WHERE id = 4;
-- UPDATE workers SET card_id = 5 WHERE id = 5;
-- UPDATE workers SET card_id = 6 WHERE id = 6;
-- UPDATE workers SET card_id = 7 WHERE id = 7;
-- UPDATE workers SET card_id = 8 WHERE id = 8;
-- UPDATE workers SET card_id = 9 WHERE id = 9;
-- UPDATE workers SET card_id = 10 WHERE id = 10;
-- UPDATE workers SET card_id = 11 WHERE id = 11;
--
-- SELECT
--     *
-- FROM workers w
--          JOIN cards c ON w.card_id = c.id
-- WHERE c.status_id = 1
--UPDATE workers SET updated_time = extract(epoch from NOW()) WHERE id = 1;
--UPDATE workers SET updated_time = date_part('epoch', TIMESTAMP 'now') WHERE id = 1;